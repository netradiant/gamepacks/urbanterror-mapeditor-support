# Urban Terror map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Urban Terror.

This gamepack is based on the game pack provided by NetRadiant-custom.

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/UrTPack/ in the future.
